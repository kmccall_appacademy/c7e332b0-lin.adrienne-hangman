class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    word_length = @referee.pick_secret_word.to_i
    @guesser.register_secret_length(word_length)
    @board = Array.new(word_length)
  end

  def play
    setup
    until won?
      display_board
      take_turn
    end

    puts 'The Guesser Wins!'
  end

  def display_board
    print 'Secret word: '
    @board.each do |ch|
      ch ? (print ch) : (print '_')
    end
    puts
    puts
  end

  def take_turn
    letter = @guesser.guess(@board)
    indices = @referee.check_guess(letter)
    update_board(letter, indices)
    @guesser.handle_response(letter, indices)
  end

  def update_board(letter, indices)
    indices.each { |idx| @board[idx] = letter }
  end

  def won?
    @board.all?
  end

end


class HumanPlayer

  def pick_secret_word
    print "Pick a secret word. Type the length of that word: "
    @secret_word_length = gets.chomp
  end

  def check_guess(guess)
    puts "Your opponent guessed '#{guess}'."
    puts "If the letter is in your word, type where it occurs."
    puts "For example, a guess of 'l' for 'hello' would be: '3,4'"
    puts "If the guess is not in your word, type 0."
    input = gets.chomp

    if input == "0"
      indices = []
    else
      indices = input.split(',').map { |num| num.to_i - 1 }
    end

    indices
  end

  def register_secret_length(length)
    puts "This word has #{length} letters."
  end

  def guess(_board)
    puts "Guess a letter: "
    @guess = gets.chomp
  end

  def handle_response(letter, indices)
    if indices.empty?
      puts "'#{letter}' is not in the word. Try again."
    else
      puts "Nice job, #{letter} is in the word."
      puts
    end
  end

end

class ComputerPlayer

  attr_accessor :candidate_words

  def initialize(dictionary = File.readlines('dictionary.txt'))
    @dictionary = dictionary.map { |line| line.chomp}
  end

  def pick_secret_word
    @secret_word = @dictionary.sample.chomp
    @secret_word.length
  end

  def check_guess(letter)
    #raise 'Invalid guess' unless letter =~ /\w/

    indices = []
    @secret_word.chars.each_index do |idx|
      indices << idx if @secret_word[idx] == letter.downcase
    end

    indices
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)
    counter = Hash.new(0)

    @candidate_words.each do |word|
      word.each_char do |ch|
        counter[ch] += 1
      end
    end
    counter.delete_if { |ch, _| board.include?(ch) }

    guess = counter.sort_by { |_, count| count }[-1][0]

    guess
  end

  def handle_response(letter, indices)
    if indices.empty?
      @candidate_words.reject! { |word| word.include?(letter) }
    else
      @candidate_words.each do |word|
        check_word(word, letter, indices)
      end
    end
  end

  def check_word(word, letter, indices)
    actual_indices = []
    word.chars.each_index do |idx|
      actual_indices << idx if word[idx] == letter
    end

    indices.each do |idx|
      if word[idx]
        @candidate_words.delete(word) unless actual_indices == indices
      end
    end
  end

end

if __FILE__ == $PROGRAM_NAME
  puts "Who is guessing? Type 'c' for computer or 'h' for human: "
  input = gets.chomp
  input == 'c' ? guesser = ComputerPlayer.new : guesser = HumanPlayer.new
  puts "Who is the referee? Type 'c' for computer or 'h' for human: "
  input = gets.chomp
  input == 'c' ? referee = ComputerPlayer.new : referee = HumanPlayer.new

  Hangman.new({guesser: guesser, referee: referee}).play
end
